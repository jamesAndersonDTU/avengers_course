# Adapt Framework Upgraded 

## What's New in Adapt Upgraded Course? 

The following components were added into the new Upgraded Course. Basic Course had a base theme, and a handful of components that received incremental updates sparatically. 

With the Adapt Upgraded Courses Chris Hintz has gone above and beyond by utilizing all the components used to build courses and ensured that they all work within the Adapt Framework Version 3.0.

##Components list are: 
+ Accordion 
+ assessmentResults
+ Blank - When you just need a little blank space
+ GMCQ - Graphic Multiple Choice Question
+ Graphic - Uses some audio, and main for showing images.
+ Hotgraphic - a more attractive graphic component
+ Matching
+ MCQ - Multiple Choice Question
+ Media
+ Narrative 
+ Slider
+ Text
+ Text-input
+ Greensock - an HTML component that allows you to put games, animations, or specific html elements needed to run within Adapt Framework 
+ Hotgrid - Similar to Narrative creates several images with on, over, and visited states and capturing that interaction with users easier and more visible.
+ Kaltura - our videos and main component for embeding into a course. 
+ Next-button - Allows with trickle to put a continue/next option between the various articles or modules. 

## Extensions
Additionally there are Extensions that have been updated with this version of Adapt that work intangibly with the Components

+ DT Audio - our main extension that adds the audio to components. Example adding audio (extension) to a Narrative (component)
+ Trickle - Uses with Scorm to allow access to certain parts of the course without allowing them to push through unless they complete specific examples or exercises. Click throughs, navigations, pop ups, video plays, game completions, multiple choice answered it will track and keep most users from surpassing the course without fully completing all aspects of it. 
+ Page Incomplete Prompt - a newer extension provides a "HEY YOU MISSED SOMETHING" prompt as they navigate away from modules. Gives a friendly FOMO before they can leave the course. Bookmakring also helps with remembering their last position in course, and Prompt works to remind them that they are not finished. 

As new components, extensions and features get worked in or adjusted. This course template should be utilized and maintanined. Check commits to see what has been updated or adjusted since last. 

Always clone to your desktop or create a branch. Never work off the master. 

If you have any questions feel free to reach out to Jacob Hayslett, Chris Hintz or Kyle Mooney

[![Join the chat at https://gitter.im/adaptlearning/adapt_framework](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/adaptlearning/adapt_framework?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)
Adapt is a free and easy to use framework and authoring tool that creates fully responsive, multi-device, HTML5 e-learning content.

**Important Note:** Adapt no longer supports IE8, 9 & 10. If you need support for those browsers, please use [the legacy branch of Adapt](https://github.com/adaptlearning/adapt_framework/tree/legacy) instead.

## What Does It Do?
The Adapt framework is a toolkit for creating responsive, single-version HTML5 e-learning courses for delivery on a web server or SCORM-compliant learning management system.

We've also created a [server-based authoring tool](https://github.com/adaptlearning/adapt_authoring/) for creating courses using the framework.

Interested? [See what Adapt can do.](https://community.adaptlearning.org/demo2/index.html)

## Features
+ Open source, published under the GNU General Public License, and FREE
+ HTML5 and CSS3
+ Responsive across multiple devices and platforms
+ Deliver courses through SCORM compliant LMS or stand-alone
+ 22 bundled plug-ins, [more available](https://www.adaptlearning.org/index.php/plugin-browser/) through the community 
+ [Meets WAI AA accessibility standards](https://github.com/adaptlearning/adapt_framework/wiki/Accessibility)
+ Right-To-Left compatibility
+ Assessments with support for question banks, randomisation, and customisable feedback
+ [Step locking](https://github.com/adaptlearning/adapt_framework/wiki/Locking-objects-with-'_isLocked'-and-'_lockType') and bookmarking
+ [Multi language and localisation support](https://github.com/adaptlearning/adapt_framework/wiki/Course-Localisation)
+ Customisable theme

Got five minutes to spare? Check out [our whistle-stop tour](https://github.com/adaptlearning/adapt_framework/wiki/Framework-in-five-minutes) of the framework to find out more.

## Full Documentation
[Visit the wiki](https://github.com/adaptlearning/adapt_framework/wiki) for full documentation, including **installation**, course authoring, developer guides, and other information.

## Communication
+ [Twitter: @AdaptLearning](https://twitter.com/adaptlearning)
+ [Gitter](https://gitter.im/orgs/adaptlearning/rooms)
+ [Community Site](https://community.adaptlearning.org/)
+ [Technical Forum](https://community.adaptlearning.org/mod/forum/view.php?id=4)
+ [Bugs / Feature Requests](https://github.com/adaptlearning/adapt_framework/issues/new)
+ [Responsive e-Learning Forum](https://community.adaptlearning.org/mod/forum/view.php?id=56)

## Contributing to Adapt
See [Contributing to the Adapt Framework](https://github.com/adaptlearning/adapt_framework/wiki/Contributing-to-the-Adapt-Project).

## Troubleshooting, Bugs, and Feedback
+ For help with troubleshooting, visit the [Technical Discussion Forum](https://community.adaptlearning.org/mod/forum/view.php?id=4).
+ To report a bug, please [submit an issue via Github Issues](https://github.com/adaptlearning/adapt_framework/issues/new?title=please%20enter%20a%20brief%20summary%20of%20the%20issue%20here&body=please%20provide%20a%20full%20description%20of%20the%20problem,%20including%20steps%20on%20how%20to%20replicate,%20what%20browser(s)/device(s)%20the%20problem%20occurs%20on%20and,%20where%20helpful,%20screenshots.).
+ To provide feedback, please use [GitHub Issues](https://github.com/adaptlearning/adapt_framework/issues/new).

## License
<a href="https://community.adaptlearning.org/" target="_blank"><img src="https://github.com/adaptlearning/documentation/blob/master/04_wiki_assets/plug-ins/images/adapt-logo-mrgn-lft.jpg" alt="adapt learning logo" align="right"></a>  Adapt is licensed under the [GNU General Public License, Version 3](https://github.com/adaptlearning/adapt_framework/blob/master/LICENSE).
