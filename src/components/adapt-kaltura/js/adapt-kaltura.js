define(function(require) {

  var ComponentView = require('coreViews/componentView');
  var Adapt = require('coreJS/adapt');
//  var this.kdp;

  //var x;


  var Kaltura = ComponentView.extend({
      node: "",       // This is the placeholder for 'this' reference
      videoDivId: "", // Kaltura video id varibale
      completeModuleOnVideoEnd: false,
      preRender: function() {
          this.checkIfResetOnRevisit();
      },

      postRender: function() {
          this.setReadyStatus();
          this.setupInview();
          this.setUpKaltura(this);
      },
      setUpKaltura: function(self){
        var playerId = this.model.get('containerID');
        kWidget.addReadyCallback(function(playerId) {
          var kdp = document.getElementById(playerId);
          mw.setConfig('EmbedPlayer.EnableFullscreen', true);
          kdp.kBind('mediaReady', function() {
            // Seek to 30 seconds from the start of the video
            console.log("Media Ready");
            self.videoConfig();
          })
        });
      },

      setupInview: function() {
          var selector = this.getInviewElementSelector();
          if (!selector) {
              //this.setCompletionStatus();
          } else {
              this.model.set('inviewElementSelector', selector);
              this.$(selector).on('inview', _.bind(this.inview, this));
          }
      },

      setVideoComplete: function(){
        this.node = this;
        this.videoDivId = this.model.get('containerID');
        this.completeModuleOnVideoEnd = this.model.get('completeOnEnd');

        this.completeVideo(this.node, this.videoDivId, this.completeModuleOnVideoEnd);
      },

      completeVideo: function(node, videoDivId, completeOnEnd){
        document.getElementById(videoDivId).kBind('playerPlayEnd.myfunc', function(e){
          node.setCompletionStatus();
          if(completeOnEnd){
              pipwerks.SCORM.data.set("cmi.core.lesson_status", "completed");
          }
        });
      },
      setVideoPlayAction: function(){
        this.node = this;
        this.videoDivId = this.model.get('containerID');

        function videoPlayAction(node, videoDivId){
          document.getElementById(videoDivId).kBind('playerStateChange.myPlay', function(e){
            var audiofiles = document.getElementsByTagName("audio");
            for(var i = 0; i < audiofiles.length; i++){
              audiofiles[0].pause();
            }
            var videofiles = $(".kaltura-video");
            for(var i = 0; i < videofiles.length; i++){
              if(videofiles[i].id !== videoDivId){
                var temp = document.getElementById(videofiles[i].id);
                temp.sendNotification("doPause");
              }
            }
        });
      }
      videoPlayAction(this.node, this.videoDivId);

      },
      videoConfig: function(){
        this.setVideoComplete();
        this.setVideoPlayAction();
      },
      /**
       * determines which element should be used for inview logic - body, instruction or title - and returns the selector for that element
       */
      getInviewElementSelector: function() {
          if(this.model.get('body')) return '.component-body';

          if(this.model.get('instruction')) return '.component-instruction';

          if(this.model.get('displayTitle')) return '.component-title';

          return null;
      },

      checkIfResetOnRevisit: function() {
          var isResetOnRevisit = this.model.get('_isResetOnRevisit');

          // If reset is enabled set defaults
          if (isResetOnRevisit) {
              this.model.reset(isResetOnRevisit);
          }

      },

      inview: function(event, visible, visiblePartX, visiblePartY) {
          if (visible) {
              if (visiblePartY === 'top') {
                  this._isVisibleTop = true;
              } else if (visiblePartY === 'bottom') {
                  this._isVisibleBottom = true;
              } else {
                  this._isVisibleTop = true;
                  this._isVisibleBottom = true;
              }
              if (this._isVisibleTop && this._isVisibleBottom) {
                  this.$(this.model.get('inviewElementSelector')).off('inview');
                //  this.setCompletionStatus();
              }
          }
      },

      remove: function() {
          if(this.model.has('inviewElementSelector')) {
              this.$(this.model.get('inviewElementSelector')).off('inview');
          }

          ComponentView.prototype.remove.call(this);
      }
  },
  {
      template: 'kaltura'
  });

  Adapt.register('kaltura', Kaltura);

  return Kaltura;
});
