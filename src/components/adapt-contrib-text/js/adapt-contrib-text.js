define(function(require) {

  var ComponentView = require('coreViews/componentView');
  var Adapt = require('coreJS/adapt');

  var Text = ComponentView.extend({

    preRender: function() {
      this.checkIfResetOnRevisit();
    },

    postRender: function() {
      this.setReadyStatus();

      this.setupInview();
    },

    setupInview: function() {
      var selector = this.getInviewElementSelector();

      if (!selector) {
        this.setCompletionStatus();
      } else {
        this.model.set('inviewElementSelector', selector);
        this.$(selector).on('inview', _.bind(this.inview, this));
      }
    },

    /**
     * determines which element should be used for inview logic - body, instruction or title - and returns the selector for that element
     */
    getInviewElementSelector: function() {
      if (this.model.get('body')) return '.component-body';

      if (this.model.get('instruction')) return '.component-instruction';

      if (this.model.get('displayTitle')) return '.component-title';

      return null;
    },

    checkIfResetOnRevisit: function() {
      var isResetOnRevisit = this.model.get('_isResetOnRevisit');

      // If reset is enabled set defaults
      if (isResetOnRevisit) {
        this.model.reset(isResetOnRevisit);
      }
    },

    inview: function(event, visible, visiblePartX, visiblePartY) {
      if (visible) {
        if (visiblePartY === 'top') {
          this._isVisibleTop = true;
        } else if (visiblePartY === 'bottom') {
          this._isVisibleBottom = true;
        } else {
          this._isVisibleTop = true;
          this._isVisibleBottom = true;
        }

        if (this._isVisibleTop && this._isVisibleBottom) {
          this.$(this.model.get('inviewElementSelector')).off('inview');
          this.setCompletionStatus();
          if (this.model.get("forceCourseComplete") == true) {
            pipwerks.SCORM.data.set("cmi.core.lesson_status", "completed");
            console.log("You have completed the Store Ops module");

            var student_id = pipwerks.SCORM.get("cmi.core.student_id")+"@discounttire.com";
            var student_name = pipwerks.SCORM.get("cmi.core.student_name");
            /**
             * xAPI Intergration
             *
             */
            var tincan = new TinCan({
              recordStores: [{
                endpoint: "https://lrs.tagdatasolutions-webhosting.com/grassblade-lrs/xAPI/",
                username: "17-4f865321e7898bc",
                password: "b9d828bb12626b447fce1cf97",
                allowFail: false
              }]
            });

            // xAPI Activity name
            var activity_id = "http://discounttire.com/tincan/activities/intro-to-store-operations/completed";

            tincan.sendStatement({
                "actor": {
                  "mbox": "mailto:" + student_id,
                  "name": student_name,
                  "objectType": "Agent"
                },
                "verb": {
                  "id": "http://adlnet.gov/expapi/verbs/completed",
                  "display": {
                    "en-US": "completed"
                  }
                },
                "result": {
                  "completion": true,
                },
                "object": {
                  "id": activity_id,
                  "objectType": "Activity",
                  "definition": {
                    "name": {
                      "en-US": "Intro to Store Operations"
                    }
                  }
                }
              },
              function(err, result) {
                //Handle any errors here. This code just outputs the result to the page.
                console.log(err);
                console.log(result);
                console.log("xAPI Statement Sent :: COURSE COMPLETED");

              }
            );
          }
        }
      }
    },

    remove: function() {
      if (this.model.has('inviewElementSelector')) {
        this.$(this.model.get('inviewElementSelector')).off('inview');
      }

      ComponentView.prototype.remove.call(this);
    }
  }, {
    template: 'text'
  });

  Adapt.register('text', Text);

  return Text;
});
